module.exports = auth;

const loginController = require("../../controllers/api/auth/login");
const loginValidation = require("../../validations/auth/login");

const registerController = require("../../controllers/api/auth/register");
const registerValidation = require("../../validations/auth/register");

function auth(app) {
  app.post("/login", loginValidation, loginController);
  app.post("/register", registerValidation, registerController);
}
