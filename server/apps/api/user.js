module.exports = user;

const show = require("../../controllers/api/user/show");
const auth = require("../../middlewares/auth/token");
const sudo = require("../../middlewares/superuser");

function user(app) {
  app.get("/show/:id([0-9]+)", (req, res) => show({ req, res, app }));
  app.post("/", [auth, sudo], (req, res) => res.json(req.user));
}
