module.exports = controller;

const bcrypt = require("bcrypt");

async function controller({ user, body, models }, res) {
  if (bcrypt.compareSync(body.password, user.password)) {
    const { UserSession } = models;
    const { id, name, email, createdAt } = user;
    const token = await UserSession.token();

    user.createSession({ token });

    return res.json({
      id,
      name,
      email,
      token,
      createdAt
    });
  }

  return res.status(400).json({ error: "wrong password" });
}
