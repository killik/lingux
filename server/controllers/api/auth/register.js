module.exports = controller;

const bcrypt = require("bcrypt");

async function controller({ models, body }, res) {
  const { User, UserSession } = models;
  const { name, email, password } = body;

  User.create(
    {
      name,
      email,
      password: bcrypt.hashSync(password, 10),
      sessions: {
        token: await UserSession.token()
      }
    },
    {
      include: [
        {
          model: UserSession,
          as: "sessions"
        }
      ]
    }
  )
    .then(({ id, name, email, createdAt, sessions }) => {
      const { token } = sessions[0];
      return res.json({
        id,
        name,
        email,
        token,
        createdAt
      });
    })

    .catch(err => res.status(500).json(err));
}
