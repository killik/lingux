module.exports = show;

function show({ req, res }) {
  const id = req.params.id;

  req.models.User.findOne({
    where: {
      id
    }
  }).then(data => {
    if (data === null) {
      res.status(404).send();
    } else {
      console.log(data);
    }
  });
}
