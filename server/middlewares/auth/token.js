module.exports = middleware;

function middleware(req, res, next) {
  const { User, UserSession } = req.models;
  const token = req.get("authorization");

  UserSession.findOne({
    where: { token },
    attributes: ["id"],
    include: [
      {
        model: User,
        attributes: ["id", "name", "email", "createdAt", "updatedAt"],
        as: "user",
        include: ["roles", "permissions"]
      }
    ]
  }).then(session =>
    session && (req.user = session.user)
      ? next()
      : res
          .status(401)
          .json({ message: "invalid or missing authorization Token" })
  );
}
