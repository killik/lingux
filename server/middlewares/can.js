module.exports = middleware;

function middleware(permissionName) {
  return ({ user }, res, next) => {
    const error =
      (!user && "authentication Required!") ||
      (!user.can(permissionName) && "you dont have Permissions");

    return !error ? next() : res.status(401).json({ message: error });
  };
}
