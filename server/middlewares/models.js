module.exports = middleware;

function middleware(req, _res, next) {
  req.models = req.models || require("../models");
  return next();
}
