module.exports = middleware;

function middleware(roleName) {
  return ({ user }, res, next) => {
    const error =
      (!user && "authentication Required!") ||
      (!user.role(roleName) && "you dont have Permissions");

    return !error ? next() : res.status(401).json({ message: error });
  };
}
