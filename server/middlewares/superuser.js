module.exports = middleware;

async function middleware({ user }, res, next) {
  const error =
    (!user && "authentication Required!") ||
    (!(await user.role("super user")) && "you dont have Permissions");

  return !error ? next() : res.status(401).json({ message: error });
}
