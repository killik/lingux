"use strict";
module.exports = (sequelize, DataTypes) => {
  const Permission = sequelize.define(
    "Permission",
    {
      name: DataTypes.STRING
    },
    {}
  );
  Permission.associate = function({ User, Role, UserRole, UserPermission }) {
    Permission.belongsToMany(User, {
      through: UserPermission,
      as: "users"
    });
    Permission.belongsToMany(Role, {
      through: UserRole,
      as: "roles"
    });
  };
  return Permission;
};
