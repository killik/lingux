"use strict";
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define(
    "Role",
    {
      name: DataTypes.STRING
    },
    {}
  );
  Role.associate = function({ User, Permission, UserRole, RolePermission }) {
    Role.belongsToMany(Permission, {
      through: RolePermission,
      as: "permissions"
    });
    Role.belongsToMany(User, { through: UserRole, as: "users" });
  };
  return Role;
};
