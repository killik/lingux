"use strict";
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING
    },
    {}
  );

  User.methods = ({ Permission, Role }) => {
    const methods = {
      async can(permissionName) {
        const permission = await Permission.findOne({
          where: { name: permissionName }
        });
        return !!(await this.hasPermissions(permission));
      },

      async role(roleName) {
        const role = await Role.findOne({
          where: { name: roleName }
        });
        return !!(await this.hasRoles(role));
      }
    };

    return methods;
  };

  User.associate = ({
    UserPermission,
    Role,
    Permission,
    UserRole,
    UserSession
  }) => {
    User.hasMany(UserSession, { as: "sessions" });
    User.belongsToMany(Role, { through: UserRole, as: "roles" });
    User.belongsToMany(Permission, {
      through: UserPermission,
      as: "permissions"
    });
  };
  return User;
};
