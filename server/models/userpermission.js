"use strict";
module.exports = (sequelize, DataTypes) => {
  const UserPermission = sequelize.define(
    "UserPermission",
    {
      userId: DataTypes.INTEGER,
      permissionId: DataTypes.INTEGER
    },
    {}
  );
  UserPermission.associate = function(models) {
    // associations can be defined here
  };
  return UserPermission;
};
