"use strict";

const randomString = require("../../helpers/string/random");

module.exports = (sequelize, DataTypes) => {
  const UserSession = sequelize.define(
    "UserSession",
    {
      userId: DataTypes.INTEGER,
      token: DataTypes.STRING
    },
    {}
  );

  UserSession.token = async () => {
    const token = randomString(255);

    const session = await UserSession.findOne({
      where: { token },
      attriutes: ["token"]
    });

    return !session ? token : UserSession.token();
  };

  UserSession.associate = function({ User }) {
    UserSession.belongsTo(User, {
      as: "user"
    });
  };
  return UserSession;
};
