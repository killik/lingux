"use strict";

const { hashSync } = require("bcrypt");

const { User, Role } = require("../models");
const { name, email, password } = require("../config/superuser.json");

module.exports = {
  up: () => {
    return User.create(
      {
        name,
        email,
        password: hashSync(password, 10),
        roles: [{ name: "super user" }]
      },
      {
        include: ["roles"]
      }
    );
  },

  down: () => {
    return User.delete({ where: { email } });
  }
};
