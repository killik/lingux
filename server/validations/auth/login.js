module.exports = middleware;

async function middleware(req, res, next) {
  const { body, models } = req;
  const { User } = models;

  let error;
  ["email", "password"].forEach(
    field => (error = !body[field] && `${field} is Required!`)
  );

  if (!error) {
    const { email } = body;

    req.user = await User.findOne({ where: { email } });

    error = !req.user && `email: ${email} not Found.`;
  }

  return error ? res.status(400).json({ error }) : next();
}
