module.exports = middleware;

async function middleware({ body, models }, res, next) {
  const { User } = models;

  let error;

  ["name", "email", "password"].forEach(
    field => (error = !body[field] && `${field} is Required!`)
  );

  if (!error) {
    const { email } = body;
    const user = await User.findOne({
      where: { email },
      attributes: ["email"]
    });

    error = user && `email: ${email} already Exists`;
  }

  return error ? res.status(400).json({ error }) : next();
}
